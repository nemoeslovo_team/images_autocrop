PNG_MASK = ".png"
__author__ = 'dan'

import Image
import ImageChops
import glob
import os


def scale_images_in_path(path) :

    file_listing = os.listdir(path)
    file_listing.sort()

    i = 0
    for image_path in file_listing :
        full_path = path + os.path.sep + image_path
        # if (os.path.isdir(full_path)) :
        #     scale_images_in_path(full_path, scale_factor)

        if PNG_MASK in image_path :
             i += 1
             im         = Image.open(full_path)
             im         = autocrop(im, (255, 255, 255, 0))
             im         = autocrop(im, (0, 0, 0, 0))
             im         = im.save(full_path)

def autocrop(im, bgcolor):

    if im.mode != "RGBA":
        im = im.convert("RGBA")
    bg = Image.new("RGBA", im.size, bgcolor)
    diff = ImageChops.difference(im, bg)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)
    return None # no contents

def main() :
    import sys
    if len(sys.argv) < 2 :
        print ("you must provide an images dir path for script")
        sys.exit()

    print(sys.argv)
    script_path   = os.path.normpath(sys.argv[1])

    scale_images_in_path(script_path)

if __name__ == "__main__" :
    main()